import numpy as np
import matplotlib.pyplot as plt
from neural_wrappers.utilities import getGenerators

def inference_dataset(models, reader):
    modelDepth, modelSemantic = models
    generator, numSteps, valGenerator, valNumSteps = getGenerators(reader, batchSize=10)

    modelDepth, modelSemantic = models
    items = next(generator)["data"]
    rgb, sema, dph = items["rgb"], items["semantic_segmentation"], items["depth"]

    cmap = np.load("semantic_cmap.npy")

    for j in range(len(items["rgb"])):
        rgb, sema, dph = items["rgb"][j], items["semantic_segmentation"][j], items["depth"][j]  
        gtSemantic = cmap[sema.argmax(axis=2).astype(np.uint8)]
        rgb = (rgb * 255).astype(np.uint8)
        dph = dph[..., 0]

        yDepth = modelDepth.doInference(rgb)[1]
        ySemantic = modelSemantic.doInference(rgb)
        ySemantic = cmap[ySemantic.argmax(axis=2).astype(np.uint8)]

        ax = plt.subplots(2, 3, figsize=(12, 6))[1]
        ax[0, 0].imshow(rgb)
        ax[0, 1].imshow(gtSemantic)
        ax[0, 2].imshow(dph)
        ax[1, 1].imshow(ySemantic)
        ax[1, 2].imshow(yDepth)
        [axi.set_axis_off() for axi in ax.ravel()]
        plt.savefig("%d.png" % j)

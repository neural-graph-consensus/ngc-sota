import sys
import numpy as np
import matplotlib.pyplot as plt
import torch as tr
from functools import partial
from argparse import ArgumentParser
from PIL import Image
import cv2

from neural_wrappers.utilities import resize
from neural_wrappers.pytorch import device

def oof():
    oof = {'NAME': 'nyudv2', 'RGB_PIXEL_MEANS': (0.485, 0.456, 0.406), 'RGB_PIXEL_VARS': (0.229, 0.224, 0.225), 'DEPTH_SCALE': 10.0, 'CROP_SIZE': (385, 385), 'DEPTH_MIN': 0.01, 'DEPTH_MAX': 1.7, 'DEPTH_MIN_LOG': -2.0, 'DEPTH_BIN_INTERVAL': 0.014869659475855161, 'DEPTH_BIN_BORDER': None, 'WCE_LOSS_WEIGHT': None, 'FOCAL_X': 519.0, 'FOCAL_Y': 519.0}
    C = 150 #__C.MODEL.DECODER_OUTPUT_C

    oof["DEPTH_MIN_LOG"] = np.log10(oof["DEPTH_MIN"])
    # Modify some configs
    oof["DEPTH_BIN_INTERVAL"] = (np.log10(oof["DEPTH_MAX"]) - np.log10(
        oof["DEPTH_MIN"])) / C

    # The boundary of each bin
    oof["DEPTH_BIN_BORDER"] = np.array(
        [np.log10(oof["DEPTH_MIN"]) + oof["DEPTH_BIN_INTERVAL"] * (i + 0.5)
         for i in range(150)])
    oof["WCE_LOSS_WEIGHT"] = [[np.exp(-0.2 * (i - j) ** 2) for i in range(C)]
                                   for j in np.arange(C)]
    return oof

    # for k, v in vars(train_args).items():
    #     if k.upper() in __C.TRAIN.keys():
    #         __C.TRAIN[k.upper()] = getattr(train_args, k)

def bins_to_depth(depth_bin):
    """
    Transfer n-channel discrate depth bins to 1-channel conitnuous depth
    :param depth_bin: n-channel output of the network, [b, c, h, w]
    :return: 1-channel depth, [b, 1, h, w]
    """
    if type(depth_bin).__module__ != tr.__name__:
        depth_bin = tr.tensor(depth_bin, dtype=tr.float32).cuda()
    depth_bin = depth_bin.permute(0, 2, 3, 1) #[b, h, w, c]
    binBorder = oof()["DEPTH_BIN_BORDER"]
    if type(binBorder).__module__ != tr.__name__:
        binBorder = tr.tensor(binBorder, dtype=tr.float32).cuda()
    depth = depth_bin * binBorder
    depth = tr.sum(depth, dim=3, dtype=tr.float32, keepdim=True)
    depth = 10 ** depth
    depth = depth.permute(0, 3, 1, 2)  # [b, 1, h, w]
    return depth

def scale(img):
    img = img.astype(np.float32)
    RGB_PIXEL_MEANS = (0.485, 0.456, 0.406)  # (102.9801, 115.9465, 122.7717)
    RGB_PIXEL_VARS = (0.229, 0.224, 0.225)  # (1, 1, 1)
    img = (img / 255 - RGB_PIXEL_MEANS) / RGB_PIXEL_VARS
    img = img[:, :, ::-1].astype(np.float32).copy()
    img = img.astype(np.float32)
    return img.copy()

def doInference(model, img):
    img_scaled = scale(img).transpose(2, 0, 1)[None]

    model.eval()
    _, pred_depth_softmax= model.depth_model.npForward(img_scaled)
    print(pred_depth_softmax.mean(), pred_depth_softmax.std())
    pred_depth = bins_to_depth(pred_depth_softmax)
    pred_depth = pred_depth.cpu().numpy().squeeze()
    pred_depth_scale = (pred_depth / pred_depth.max() * 60000).astype(np.uint16)  # scale 60000 for visualization
    return pred_depth, pred_depth_scale

def getModel():
    from models.metric_depth_model import MetricDepthModel
    model = MetricDepthModel().to(device)
    model.doInference = lambda x : doInference(model, x)
    return model

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("imagePath")
    parser.add_argument("weightsPath")
    return parser.parse_args()

def main():
    args = getArgs()
    model = getModel()
    model.loadWeights(args.weightsPath)

    # img = np.array(Image.open(args.imagePath))
    img = cv2.imread(args.imagePath)
    pred_depth, pred_depth_scale = doInference(model, img)

    fig = plt.subplots(1, 2)[1]
    fig[0].imshow(img[:,:,::-1])
    fig[1].imshow(pred_depth_scale)
    plt.show()

if __name__ == "__main__":
    main()
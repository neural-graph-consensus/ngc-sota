import torch
import cv2
import numpy as np
import torch.nn.functional as F


def resize_image(img, size):
    if type(img).__module__ != np.__name__:
        img = img.cpu().numpy()
    img = cv2.resize(img, (size[1], size[0]))
    return img


def kitti_merge_imgs(left, middle, right, img_shape, crops):
    """
    Merge the splitted left, middle and right parts together.
    """
    left = torch.squeeze(left)
    right = torch.squeeze(right)
    middle = torch.squeeze(middle)
    out = torch.zeros(img_shape, dtype=left.dtype, device=left.device)
    crops = torch.squeeze(crops)
    band = 5

    out[:, crops[0][0]:crops[0][0] + crops[0][2] - band] = left[:, 0:left.size(1)-band]
    out[:, crops[1][0]+band:crops[1][0] + crops[1][2] - band] += middle[:, band:middle.size(1)-band]
    out[:, crops[1][0] + crops[1][2] - 2*band:crops[2][0] + crops[2][2]] += right[:, crops[1][0] + crops[1][2] - 2*band-crops[2][0]:]

    out[:, crops[1][0]+band:crops[0][0] + crops[0][2] - band] /= 2.0
    out[:, crops[1][0] + crops[1][2] - 2*band:crops[1][0] + crops[1][2] - band] /= 2.0
    out = out.cpu().numpy()

    return out
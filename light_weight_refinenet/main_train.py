import numpy as np
import torch as tr
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from argparse import ArgumentParser
from functools import partial
from neural_wrappers.utilities import getGenerators, resize_batch, toCategorical
from neural_wrappers.readers import NYUDepthV2H5PathsReader
from neural_wrappers.callbacks import SaveModels, PlotMetrics, SaveHistory
from neural_wrappers.metrics import Accuracy, MeanIoU

from model import getModel

def semanticSegmentationNorm(x : np.ndarray, readerObj : NYUDepthV2H5PathsReader) -> np.ndarray:
	x = x.astype(np.uint8)
	x = resize_batch(x, interpolation="nearest", height=120, width=160, resizeLib="opencv")
	x = x[..., 0]
	x[x == 255] = 0
	x = toCategorical(x, numClasses=40)
	x = x.reshape(-1, 120, 160, 40).astype(np.bool)
	x = x.transpose(0, 3, 1, 2)
	return x

def rgbNorm(x, readerObj):
	IMG_MEAN = np.array([0.485, 0.456, 0.406], dtype=np.float32)
	IMG_STD = np.array([0.229, 0.224, 0.225], dtype=np.float32)
	x = (x - IMG_MEAN) / IMG_STD
	x = x.transpose(0, 3, 1, 2)
	return x

class Reader(NYUDepthV2H5PathsReader):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.dimTransform["data"]["semantic_segmentation"] = partial(semanticSegmentationNorm, readerObj=self)
		self.dimTransform["data"]["rgb"] = partial(rgbNorm, readerObj=self)

	def iterateOneEpoch(self, topLevel, batchSize):
		for data in super().iterateOneEpoch(topLevel, batchSize):
			rgb = data["data"]["rgb"]
			semantic = data["data"]["semantic_segmentation"]

			yield rgb, semantic

def lossFn(y, t):
	# Negative log-likeklihood (used for softmax+NLL for classification), expecting targets are one-hot encoded
	y = F.softmax(y, dim=1)
	t = t.type(tr.bool)
	return (-tr.log(y[t] + 1e-5)).mean()

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("datasetPath")
	parser.add_argument("validationPath")
	parser.add_argument("--weightsFile")
	args = parser.parse_args()
	assert args.type in ("train", "test")
	return args

def main():
	args = getArgs()
	model = getModel()

	reader = Reader(args.datasetPath, dataBuckets={"data" : ["rgb", "semantic_segmentation"]}, \
		desiredShape=(480, 640))
	valReader = Reader(args.validationPath, dataBuckets={"data" : ["rgb", "semantic_segmentation"]}, \
		desiredShape=(480, 640))
	generator, numSteps = getGenerators(reader, batchSize=5, keys=["train"])
	valGenerator, valNumSteps = getGenerators(valReader, batchSize=5, keys=["test"])

	model.setOptimizer(optim.AdamW, lr=0.0001)
	model.setCriterion(lossFn)
	model.addMetrics({"Mean IoU (global)" : MeanIoU(mode="global"), "Accuracy" : Accuracy()})
	model.addCallbacks([SaveModels("best", "Loss"), PlotMetrics(["Loss"]), SaveModels("last", "Loss"), \
		SaveHistory("history.txt"), SaveModels("best", "Mean IoU (global)")])
	print(model.summary())

	if args.type == "train":
		model.train_generator(generator, numSteps, 100, valGenerator, valNumSteps)
	elif args.type == "test":
		model.loadWeights(args.weightsFile)
		res = model.test_generator(valGenerator, valNumSteps)
		print(res)

if __name__ == "__main__":
	main()
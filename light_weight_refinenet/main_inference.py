import sys
import numpy as np
import torch as tr
import matplotlib.pyplot as plt
from PIL import Image
from functools import partial
from argparse import ArgumentParser

from model import getModel

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("imagePath")
    parser.add_argument("weightsPath")
    return parser.parse_args()

def main():
    args = getArgs()
    model = getModel()
    model.loadWeights(args.weightsPath)
    cmap = np.load("cmap.npy")

    img = np.array(Image.open(args.imagePath))
    gtName = "%s_mask.png" % args.imagePath[0 : -4]
    gt = cmap[np.array(Image.open(gtName))]
    y = model.doInference(img)
    y = cmap[y.argmax(axis=2).astype(np.uint8)]

    ax = plt.subplots(1, 3)[1]
    ax[0].imshow(img)
    ax[1].imshow(gt)
    ax[2].imshow(y)
    plt.savefig("res.png")
    # plt.show()

if __name__ == "__main__":
    main()
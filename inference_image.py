import matplotlib.pyplot as plt
import numpy as np
from neural_wrappers.utilities import tryReadImage
from PIL import Image

def inference_image(args, models):
    img = tryReadImage(args.path)
    gtName = "%s_mask.png" % (args.path[0 : -4])
    cmap = np.load("semantic_cmap.npy")
    gtSemantic = cmap[np.array(Image.open(gtName))]

    modelDepth, modelSemantic = models
    yDepth = modelDepth.doInference(img)[1]
    ySemantic = modelSemantic.doInference(img)
    ySemantic = cmap[ySemantic.argmax(axis=2).astype(np.uint8)]

    ax = plt.subplots(2, 2, figsize=(8, 6))[1]
    ax[0, 0].imshow(img)
    ax[0, 1].imshow(gtSemantic)
    ax[1, 0].imshow(yDepth)
    ax[1, 1].imshow(ySemantic)
    # plt.show()
    plt.savefig("blah.png")

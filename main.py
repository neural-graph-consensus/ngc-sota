import sys
from argparse import ArgumentParser
from neural_wrappers.readers import NYUDepthV2H5PathsReader

sys.path.append("light_weight_refinenet")
from light_weight_refinenet.main_inference import getModel as ModelSemantic
sys.path.append("VNL_Monocular_Depth_Prediction")
from VNL_Monocular_Depth_Prediction.main_inference import getModel as ModelDepth

from inference_image import inference_image
from inference_dataset import inference_dataset

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("type")
    parser.add_argument("path")

    parser.add_argument("--singleLinksPath")
    args = parser.parse_args()

    assert args.type in ("inference_image", "inference_dataset")
    return args

def getModels(args):
    modelDepth = ModelDepth()
    modelDepth.loadWeights("%s/rgb2depth/model_weights.pkl" % args.singleLinksPath)
    print(modelDepth.summary())

    modelSemantic = ModelSemantic()
    modelSemantic.loadWeights("%s/rgb2semantic/model_weights.pkl" % args.singleLinksPath)
    print(modelSemantic.summary())
    return modelDepth, modelSemantic

def main():
    args = getArgs()

    if args.type == "inference_image":
        models = getModels(args)
        plot_image(args, models)
    elif args.type == "inference_dataset":
        models = getModels(args)

        reader = NYUDepthV2H5PathsReader(args.path, dataBuckets={
            "data" : ["rgb", "depth", "semantic_segmentation"],
        }, desiredShape=(480, 640))
        inference_dataset(models, reader)

if __name__ == "__main__":
    main()